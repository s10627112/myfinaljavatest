package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int f=0,z=0,x=0,v=0,n=0;

        String d=scanner.nextLine();
        for(int l=0; l< a;l++)
        {
            String str=scanner.nextLine();
            String[] split = str.split(" ");
            Integer[] A=new Integer[split.length];
            for(int i=0;i<split.length;i++){
                int num =   Integer.parseInt(split[i]);
                A[i] = num;
            }
            for(int b=0;b<A.length;b++) {
                if (A[b] >= 90)
                    f++;
                if (A[b] <= 89 && A[b] >= 80)
                    z++;
                if (A[b] <= 79 && A[b] >= 70)
                    x++;
                if (A[b] <= 69 && A[b] >= 60)
                    v++;
                if (A[b] <= 59)
                    n++;
            }
        }
        System.out.println(">=90:"+f+"\n"+"80-89:"+z+"\n"+"70-79:"+x+"\n"+"60-69:"+v+"\n"+"<60:"+n);
    }
}